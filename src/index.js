import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './Pages/App';
import { 
  BrowserRouter as Router,
  Switch,
  Route
 } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import Register from './Pages/Register';
import Gallery from './Pages/Gallery';
import Images from './Pages/Images';
import { responsiveFontSizes, createMuiTheme, ThemeProvider } from '@material-ui/core';
import { cyan, purple } from '@material-ui/core/colors';
import { auth } from './services/firebase'

let customTheme = responsiveFontSizes(createMuiTheme({
  palette: {
    primary: cyan,
    secondary: purple,
    type: 'dark',
  },
  typography: {
    fontFamily: [
      '"Segoe UI"',
    ]
  }
}))

ReactDOM.render(
  <ThemeProvider theme={customTheme}>
    <Router>
      <Switch>      
        <Route path="/register">
          <Register/>
        </Route>
        <Route path="/gallery">
          <Gallery />
        </Route>
        <Route path="/images">
          <Images />
        </Route>
        <Route path="/">
          <App />
        </Route>
      </Switch>
    </Router>
  </ThemeProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
