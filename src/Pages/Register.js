import React, { useState } from 'react';
import { Grid, CssBaseline, Paper, Avatar, Typography, TextField, Button, Box, Link } from '@material-ui/core';
import { responsiveFontSizes, createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import { LockOutlined } from '@material-ui/icons';
import { cyan, purple } from '@material-ui/core/colors';
import { signup } from '../helpers/auth';
import { useHistory } from 'react-router-dom';

let customTheme = responsiveFontSizes(createMuiTheme({
    palette: {
        type: 'dark',
        primary: cyan,
        secondary: purple,
      },  
      typography: {
        fontFamily: [
          '"Segoe UI"',
          'Nunito',
          'Roboto',
          '"Helvetica Neue"',
          'Arial',
          'sans-serif'
        ].join(','),
      }
}))

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {`Copyright © `}
            <Link color="inherit" href="#">
                Your Website
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    )
}

const useStyles = makeStyles(() => {
    let theme = customTheme;

    return ({
        root: {
          height: '100vh',
        },
        image: {
          backgroundImage: 'url(https://source.unsplash.com/random)',
          backgroundRepeat: 'no-repeat',
          backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
          backgroundSize: 'cover',
          backgroundPosition: 'center',
        },
        paper: {
          margin: theme.spacing(8, 4),
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        },
        avatar: {
          margin: theme.spacing(1),
          backgroundColor: theme.palette.secondary.main,
        },
        form: {
          width: '100%', // Fix IE 11 issue.
          marginTop: theme.spacing(1),
        },
        submit: {
          backgroundColor: theme.palette.primary.main,
          margin: theme.spacing(3, 0, 2),
        },
      })
});

export default function Register() {
    const classes = useStyles(customTheme);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const history = useHistory();

    const register = async (e) => {
      e.preventDefault();
      await signup(email, password).then(res => {
        history.push('/');
      })
    }

    return (
        <ThemeProvider theme={customTheme}>
            <Grid container component="main" className={classes.root}>
                <CssBaseline />
                <Grid item xs={false} sm={4} md={7} className={classes.image} />
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={10} square>
                    <div className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <LockOutlined />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Register
                        </Typography>
                        <form className={classes.form} onSubmit={(e) => register(e)}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                label="Email Address"
                                name="email"
                                autoComplete="email"
                                autoFocus
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="password"
                                label="Password"
                                name="password"
                                autoComplete="current-password"
                                type="password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                            >
                                Register
                            </Button>
                            <Box mt={5}>
                                <Copyright />
                            </Box>
                        </form>
                    </div>
                </Grid>
            </Grid>
        </ThemeProvider>
    )
}