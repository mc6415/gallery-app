import React, { useState } from 'react';
import { Button, Grid, CssBaseline, Paper, Avatar, Typography, TextField, FormControlLabel, Checkbox, Link, Box } from '@material-ui/core'
import LockOutlinedIcon  from '@material-ui/icons/LockOutlined'
import { makeStyles } from '@material-ui/core'
import { createMuiTheme, ThemeProvider, responsiveFontSizes } from '@material-ui/core/styles';
import { purple, cyan } from '@material-ui/core/colors'
import GoogleButton from 'react-google-button';
import { signInWithGoogle, signin } from '../helpers/auth';
import { auth } from '../services/firebase';
import { useHistory } from 'react-router-dom';

let customTheme = responsiveFontSizes(createMuiTheme({
  palette: {
    type: 'dark',
    primary: cyan,
    secondary: purple,
  },  
  typography: {
    fontFamily: [
      '"Segoe UI"',
      'Nunito',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif'
    ].join(','),
  }
}));

const useStyles = makeStyles(() => {
  let theme = customTheme;

  return ({
    root: {
      height: '100vh',
    },
    image: {
      backgroundImage: `url(https://source.unsplash.com/collection/1127161)`,
      backgroundRepeat: 'no-repeat',
      backgroundColor:
        theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    },
    paper: {
      margin: theme.spacing(8, 4),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      backgroundColor: theme.palette.primary.main,
      margin: theme.spacing(3, 0, 2),
    },
    googleSubmit: {
      margin: theme.spacing(3, 0, 2),
      width: '100% !important',
    }
  })
});

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  )
} 

export default function SignInSide(props) {
  const classes = useStyles(customTheme);
  const [ email, setEmail ] = useState('');
  const [ password, setPassword ] = useState('');

  let history = useHistory();

  const googleSignIn = async() => {
    await signInWithGoogle().then(res => {
      history.push('/gallery');
    })
  }

  const signInWithEmail = async(e) => {
    e.preventDefault();
    await signin(email, password).then(res => {
      history.push('/gallery');
    })
  }

  return (
    <ThemeProvider theme={customTheme}>
      <Grid container component="main" className={classes.root}>
        <CssBaseline />
        <Grid item xs={false} sm={4} md={7} className={classes.image} />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
                <Typography component="h1" variant="h5">
                  Sign In
                </Typography>        
            <form className={classes.form} noValidate onSubmit={(e) => signInWithEmail(e)}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                autoFocus
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="password"
                label="Password"
                name="password"
                type="password"
                autoComplete="current-password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Remember me"
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Sign In            
              </Button>
              <Button
                fullWidth
                variant="contained"
                color="secondary"
                href="/gallery"
              >
                Continue as Guest
              </Button>
              <GoogleButton className={classes.googleSubmit} onClick={googleSignIn} />
              <Grid container>
                <Grid item xs>
                  <Link href="#" variant="body2">
                    Forgot password?
                  </Link>
                </Grid>
                <Grid item>
                  <Link href="/register" variant="body2">
                    {"Don't have an account? Sign Up"}
                  </Link>
                </Grid>
              </Grid>
              <Box mt={5}>
                <Copyright />
              </Box>
            </form>
          </div>
        </Grid>
      </Grid>
    </ThemeProvider>
  )
}