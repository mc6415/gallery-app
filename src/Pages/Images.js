import React, { useEffect } from 'react';
import { CssBaseline, Paper, Typography, List, ListItem, Grid, ListItemText, ListItemAvatar, ListItemSecondaryAction, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { post, get } from 'axios';
import DeleteIcon  from '@material-ui/icons/Delete'
import socketIOClient from 'socket.io-client';

const endpoint = "http://94.9.187.111:3001/";

const useStyles = makeStyles((theme) => ({
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto',
        }
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3)
        }
    },
    thumbnail: {
        width: 100,
    }
}));

function getImages() {
    return get('http://94.9.187.111:3001/image/thumbs').then(res => {
        let { data } = res;

        return data;
    })
}

export default function Images(props) {
    const classes = useStyles();
    const [ images, setImages ] = React.useState([]);

    useEffect(() => {
        getImages().then(res => {
            setImages(res);
        })

        const socket = socketIOClient(endpoint);

        socket.on('imagesUpdated', () => {
            getImages().then(res => setImages(res));
        });  
    }, [])

    const deleteImage = (id) => {
        post(`http://94.9.187.111:3001/image/delete`, { id });
    }

    let imageList = images.map(image => {
        let base64str = new Buffer(image.thumbnail.data).toString('base64');

        return (
            <ListItem>
                <ListItemAvatar>                    
                    <img alt="thumbnail" className={classes.thumbnail} src={`data:image/png;base64, ${base64str}`} />
                </ListItemAvatar>
                <ListItemText>
                    { image.filename }, By: { image.author }
                </ListItemText>
                <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="delete" onClick={() => deleteImage(image._id)}>
                        <DeleteIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        )
    })

    return(
        <div>
            <CssBaseline />
            <main className={classes.layout}>
                <Paper className={classes.paper}>
                    <Typography component="h1" variant="h4" align="center">
                        Images
                    </Typography>
                    <Grid item xs={12} md={6}>
                        <div>
                            <List>
                                { imageList }
                            </List>
                        </div>
                    </Grid>
                </Paper>
            </main>
        </div>
    )
}