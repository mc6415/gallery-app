import React, { useEffect } from 'react';
import { CssBaseline, Grid, GridList, GridListTile, GridListTileBar, Fade, Button, 
    AppBar, Toolbar, IconButton, useMediaQuery, Typography } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Menu as MenuIcon } from '@material-ui/icons';
import { post, get } from 'axios';
import DeleteIcon from '@material-ui/icons/Delete'
import InfoIcon from '@material-ui/icons/ZoomInOutlined'
import socketIOClient from 'socket.io-client';
import { auth } from '../services/firebase';
import { signout } from '../helpers/auth';
import { useHistory } from 'react-router-dom';
import UploadDialog from '../components/uploadDialog';
import * as Constants from '../constants';

const useStyles = makeStyles((theme) => {
    return ({
        root: {
            height: '100vh',
        },
        gridList: {
            height: '100vh',
            width: '100%',
        },
        gridListTile: {
            padding: '8px',
        },
        title: {
            flexGrow: 1
        },        
    })
})

function useWidth() {
    const theme = useTheme();
    const keys = [...theme.breakpoints.keys].reverse();
    return (
      keys.reduce((output, key) => {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        const matches = useMediaQuery(theme.breakpoints.up(key));
        return !output && matches ? key : output;
      }, null) || 'xs'
    );
  }

function getImages() {
    return get('http://94.9.187.111:3001/image/thumbs').then(res => {
        let { data } = res;
        
        let response = data.map((image, index) => {
            var base64str = new Buffer(image.thumbnail.data).toString('base64');

            return {
                cols: 1,
                img: `data:image/png;base64, ${base64str}`,
                key: index,
                title: image.title,
                author: image.author,
                id: image._id,
                uid: image.uid
            }
        });

        return response;
    })
}

export default function Gallery(props) {
    const classes = useStyles();
    const [ loggedIn, setLoggedIn ] = React.useState(false);
    const [ open, setOpen ] = React.useState(false);
    const [ images, setImages ] = React.useState([]);

    let history = useHistory();

    useEffect(() => {       
        getImages().then(res => setImages(res));

        const socket = socketIOClient(Constants.serverEndpoint);

        auth().onAuthStateChanged((user) => {
            setLoggedIn(true);
        })

        socket.on('imagesUpdated', () => {
            getImages().then(res => setImages(res));
        });        
    }, [])
    
    let width = useWidth();
    let gridCols;

    switch(width){
        case "xl":
            gridCols = 5;
            break;
        case "lg":
            gridCols = 4;
            break;
        case "md":
            gridCols = 3;
            break;
        case "sm":
            gridCols = 2;
            break;
        case "xs":
            gridCols = 1;
            break;
        default:
            gridCols = 2;
            break;
    }

    const toggleModal = () => {
        setOpen((prev) => !prev);
    }   

    const signOut = async () => {
        await signout()
        history.push('/');
    }

    const deleteFile = (id) => {
        post('http://94.9.187.111:3001/image/delete', { id })
    }

    let imageTiles = images.map(image => {
        let canDelete = false;

        if(auth().currentUser) {
            canDelete = auth().currentUser.uid === image.uid ? true : false;
        }
        
        return (
        <Fade 
            in={true}
            timeout={Math.floor(Math.random() * 5000)}
            key={image.key}
        >
            <GridListTile key={image.key} cols={image.cols} style={{height: '50vh'}}>
                <img alt="" src={image.img} />
                <GridListTileBar
                    title={image.title}
                    subtitle={<span>by: {image.author}</span>}
                    actionIcon={
                        <>
                            {canDelete &&
                                <IconButton className={classes.icon} onClick={() => deleteFile(image.id)}>
                                    <DeleteIcon />
                                </IconButton>
                            }
                            <IconButton className={classes.icon}> 
                                <InfoIcon />
                            </IconButton>
                        </>
                    }
                />
            </GridListTile>
        </Fade>
    )})

    return (
        <>
            <AppBar>
                <Toolbar>
                    { loggedIn && 
                        <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                            <MenuIcon />
                        </IconButton> 
                    }
                    <Typography variant="h6" className={classes.title}>
                        Gallery
                    </Typography>
                    <Button variant="contained" color="secondary" onClick={toggleModal}>
                        Upload
                    </Button>
                    { !auth().currentUser && 
                        <Button color="inherit" href="/">
                            Login
                        </Button>
                    }
                    { auth().currentUser &&
                        <Button color="inherit" onClick={signOut}>
                            Logout
                        </Button>
                    }
                </Toolbar>
            </AppBar>
            <UploadDialog open={open} onClose={toggleModal} toggleModal={toggleModal} />            
            <Grid container component="main" className={classes.root}>
                <CssBaseline />
                <GridList cellHeight={100} className={classes.gridList} cols={gridCols} spacing={8}>
                    { imageTiles } 
                </GridList>
            </Grid>
        </>
    )
}