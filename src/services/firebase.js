import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyAVW8Y-LuLIxVUY-xDFpEXEoEE8_hJKqF4",
    authDomain: "gallery-tyest.firebaseapp.com",
    databaseURL: "https://gallery-tyest.firebaseio.com",
};

firebase.initializeApp(config);

export const auth = firebase.auth;
export const db = firebase.database();