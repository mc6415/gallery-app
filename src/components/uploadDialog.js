import React, { useState } from 'react';
import { Dialog, DialogTitle, DialogContent, TextField, Button, DialogActions, makeStyles } from '@material-ui/core';
import { useDropzone } from 'react-dropzone';
import { green, red } from '@material-ui/core/colors';
import * as Constants from '../constants';
import { auth } from '../services/firebase';
import { post } from 'axios';

const useStyles = makeStyles((theme) => {
    return ({
    baseStyle:  {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: '20px',
        borderWidth: 6,
        borderRadius: 2,
        borderColor: '#eeeeee',
        borderStyle: 'dashed',
        backgroundColor: '#fafafa',
        color: '#bdbdbd',
        outline: 'none',
        transition: 'border .24s ease-in-out'
    },
    activeStyle: {
        borderColor: theme.palette.primary.main,
    },
    acceptStyle: {
        borderColor: green[400],
    },
    rejectStyle: {
        borderColor: red.A700,
    },
    thumbsContainer: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 16,
    },
    thumb: {
        display: 'inline-flex',
        borderRadius: 2,
        border: '1px solid #eaeaea',
        marginBottom: 8,
        marginRight: 8,
        width: 100,
        height: 100,
        padding: 4,
        boxSizing: 'border-box'
    },
    thumbInner: {
        display: 'flex',
        minWidth: 0,
        overflow: 'hidden'
    },
    img: {
        display: 'block',
        width: 'auto',
        height: '100%',
    },
    titleInput: {
        marginBottom: '1vh'
    }
})});

export default function UploadDialog(props) {
    const { open, toggleModal } = props;    
    const classes = useStyles();
    const [ files, setFiles ] = useState([]);
    const [ title, setTitle ] = useState([]);

    const uploadFiles = () => {
        acceptedFiles.forEach(file => {
            const formData = new FormData();
            let author;

            if(!auth().currentUser) author = 'Guest';
            if(!!auth().currentUser) {
                author = auth().currentUser.displayName ? auth().currentUser.displayName : auth().currentUser.email;
            }
            formData.append('file', file);
            formData.append('author', author);
            formData.append('title', title);
            !!auth().currentUser ? formData.append('user', auth().currentUser.uid) : formData.append('user', null)

            post(`${Constants.serverEndpoint}/image/upload`, formData);
            toggleModal();
            setFiles([]);
        })
    }

    const {acceptedFiles, getRootProps, getInputProps, 
        isDragActive, isDragAccept, isDragReject} = useDropzone(
            {
            accept: 'image/*',
            onDrop: acceptedFiles => {
                setFiles(acceptedFiles.map(file => Object.assign(file, {
                    preview: URL.createObjectURL(file)
                })));
            }
        });

    const thumbs = files.map(file => (
        <div className={classes.thumb} key={file.name}>
            <div className={classes.thumbInner}>
                <img alt="" src={file.preview} className={classes.img} />
            </div>
        </div>
    ))

    return(
        <Dialog open={open}>
            <DialogTitle id="form-dialog-title">Upload</DialogTitle>
            <DialogContent>
                <form>
                    <TextField 
                        label="Title" 
                        variant="outlined"
                        value={title}
                        fullWidth
                        onChange={(e) => setTitle(e.target.value)} 
                        className={classes.titleInput}
                    />                    
                </form>
                <section className="container">
                    <div {...getRootProps({
                            className: `dropzone ${classes.baseStyle} 
                            ${isDragActive && classes.activeStyle} 
                            ${isDragAccept && classes.acceptStyle}
                            ${isDragReject && classes.rejectStyle}
                            `
                        })}>
                        <input {...getInputProps()} />
                        <p>Drag 'n' drop some file here, or click to select file</p>
                    </div>
                    <aside className={classes.thumbsContainer}>
                        {thumbs}
                    </aside>
                </section>
            </DialogContent>
            <DialogActions>
                <Button color="primary" onClick={toggleModal}>
                    Cancel
                </Button>
                <Button color="primary" onClick={uploadFiles}>
                    Upload
                </Button>
            </DialogActions>
        </Dialog>
    )
} 